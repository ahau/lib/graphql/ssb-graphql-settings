module.exports = function GetSettings (sbot) {
  return function getSettings (id, cb) {
    sbot.settings.get(id, (err, settings) => {
      if (err) return cb(err)

      cb(null, {
        ...settings,
        id
      })
    })
  }
}
