const pull = require('pull-stream')
const pullParamap = require('pull-paramap')
const { where, sortByArrival, slowEqual, toPullStream } = require('ssb-db2/operators')

module.exports = function GetAllSettings (sbot, getSettings) {
  return function getAllSettings (cb) {
    pull(
      sbot.db.query(
        where(
          slowEqual('value.content.tangles.settings.root', null)
        ),
        sortByArrival(),
        toPullStream()
      ),
      pull.map(root => root.key),
      pullParamap(getSettings, 6),
      pull.filter(settings => settings.tombstone === null),
      pull.filter(Boolean), // drop settings which has some trouble resolving
      pull.collect((err, settings) => {
        if (err) return cb(err)

        cb(null, settings)
      })
    )
  }
}
