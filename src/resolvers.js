const ssbResolvers = require('./ssb')

module.exports = function Resolvers (sbot) {
  const {
    postSaveSettings,
    getSettings,
    getAllSettings,

    gettersWithCache
  } = ssbResolvers(sbot)

  const resolvers = {
    Query: {
      settings: (_, { id }) => getSettings(id),
      allSettings: () => getAllSettings()
    },
    Mutation: {
      saveSettings: (_, { input }) => postSaveSettings(input)
    },
    PersonalIdentity: {
      settings: ({ settingsId }) => getSettings(settingsId)
    }
  }

  return {
    resolvers,
    gettersWithCache
  }
}
