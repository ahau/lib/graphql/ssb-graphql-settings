const { isMsgId } = require('ssb-ref')

function Save (apollo, t) {
  return async function save (input) {
    const res = await apollo.mutate({
      mutation: `mutation($input: SettingsInput) {
        saveSettings(input: $input)
      }`,
      variables: {
        input
      }
    })

    t.error(res.errors, 'save settings doesnt throw errors')
    t.true(isMsgId(res.data.saveSettings))
    return res.data.saveSettings
  }
}

function Get (apollo, t) {
  return async function get (id) {
    const res = await apollo.query({
      query: `query($id: ID) {
        settings(id: $id) {
          id
          keyBackedUp
          tombstone {
            date
            reason
          }
          recps
        }
      }`,
      variables: {
        id
      }
    })

    t.error(res.errors, 'get settings doesnt throw errors')

    return res.data.settings
  }
}

module.exports = {
  Save,
  Get
}
