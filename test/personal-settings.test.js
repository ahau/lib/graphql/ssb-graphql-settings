const { isMsgId } = require('ssb-ref')
const test = require('tape')
const TestBot = require('./test-bot')
const { Save } = require('./lib/helpers')

test('whoami.personal.settings', async t => {
  const { ssb, apollo } = await TestBot({ loadContext: true })

  // init reusable helpers
  const save = Save(apollo, t)

  async function whoami () {
    const res = await apollo.query({
      query: `
        query {
          whoami {
            personal {
              settings {
                id
                keyBackedUp
              }
            }
          }
        }
      `
    })

    t.error(res.errors, 'query whoami without errors')

    return res.data.whoami
  }

  const currentIdentity = await whoami()
  const settingsId = currentIdentity.personal.settings.id

  t.true(isMsgId(settingsId), 'returns valid messageId')
  t.false(currentIdentity.personal.settings.keyBackedUp, 'the keyBackedUp is initially false')

  // update the setting
  await save({
    id: settingsId,
    keyBackedUp: true
  })

  // get current identity again
  const updatedIdentity = await whoami()
  t.true(updatedIdentity.personal.settings.keyBackedUp, 'the keyBackedUp was updated')

  ssb.close()
  t.end()
})
